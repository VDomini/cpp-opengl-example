GCC_OPTIONS=-Wall -pedantic -I include
GL_OPTIONS=-lGLEW -lGL -lglut
OPTIONS=$(GCC_OPTIONS) $(GL_OPTIONS)


df: main.cpp
	g++ main.cpp Common/InitShader.o $(OPTIONS) -o $@

run:
	chmod +x main
	./main


clean:
	- rm main
