An example of OpenGL written in C++. Main code is located in main.cpp, fragment shader in fshader21, and vertex shader in vshader21.

The process is that each of the face pairs will be stored, then used to generate normals for each vector, then added to the vertex array individually, with the correct normal for each vertex having already been computed.

My camera view is pretty good. A little wonky at times, but it is capable of moving around and you can see where on the model the light is hitting based on the differently shaded green polygons. The user can stop the animation by setting the change in angle to zero through keyboard input. Basically by zeroing out the change over time. S and X change speed of rotation, R and F change radius, H and N change height of camera. I found that holding N for a second or so after starting gives you the best picture of my object and also that perspective displayed it much better, perhaps due to how near/far is handled. 

Z resets the view. Three colors cycle on click.