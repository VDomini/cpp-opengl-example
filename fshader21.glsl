#version 120

varying vec3 fN;
varying vec3 fL;
varying vec3 fL2;
varying vec3 fE;
varying vec3 color;

uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform mat4 ModelView;
uniform float Shininess;
uniform float shadeswitch;

void main()
{
    if(shadeswitch == 1)
    {
	vec3 N = normalize(fN);
    	vec3 E = normalize(fE);
    	vec3 L = normalize(fL);
    	vec3 L2 = normalize(fL2);

    	vec3 H = normalize(L + E);
    	vec3 H2 = normalize(L2 + E);
    	vec3 ambient = AmbientProduct.xyz;

    	float Kd = max(dot(L, N), 0.0) + max(dot(L2, N), 0.0);
    
	vec3 diffuse = Kd * DiffuseProduct.xyz;
    
	 float Ks = pow(max(dot(N,H),0.0), Shininess) + pow(max(dot(N,H2),0.0), Shininess);
    	 vec3 specular = Ks*SpecularProduct.xyz;

    	 if( dot(L,N) < 0.0){
	      specular = vec3(0.0,0.0,0.0);
	      }

    	 gl_FragColor = vec4(ambient + diffuse + specular, 1.0);
    }
    if( shadeswitch == 0){
	gl_FragColor = vec4(color,1.0);
    }
    if( shadeswitch == 101) {
    	 gl_FragColor = DiffuseProduct;
	 }	
}
