// First new GL program
// Just makes a red triangle

#include "Angel.h"
#include <math.h>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

const int NumPoints = 3;
static int mainwindow;
static int val = 0;
static GLfloat shading = 1;
static GLuint transLoc;
static GLuint lp0Loc;
static GLuint lp1Loc;
static GLuint projLoc;
static GLuint eyeLoc;
static GLuint ambLoc;
static GLuint difLoc;
static GLuint specLoc;
static GLuint shineLoc;
static GLuint shadeLoc;
static GLuint fbo;


static int active = 0;

static point4 points[192];
static vec3 vertexnorms[192];
static vec3 faces[384];
static point4 finalpoints[1152];

static point4 normals[1152];

//Define 3 different materials, silver, turquoise, and emerald. Emerald should have a large specular.
static int matchoose = 0;
static int matchoose1 = 1;
static int matchoose2 = 2;

static vec4 silver[3] = { vec4(0.19225, 0.19225, 0.19225, 1), vec4(0.50754, 0.50754, 0.50754, 1.0), 
			  vec4(0.508273, 0.508273, 0.508273, 1.0) } ;
static GLfloat silverShine = 51.2;

static vec4 turq[3] = {  vec4(0.1, 0.18725, 0.1745, 1), vec4(0.396, 0.74151, 0.69102, 1.0), 
			 vec4(0.297254, 0.30829, 0.306678, 1.0) } ;
static GLfloat turqShine = 12.8;

static vec4 emerald[3] = {  vec4(0.0215, 0.1745, 0.0215, 1), vec4(0.07568, 0.61424, 0.07568, 1.0), 
			    vec4(0.633, 0.727811, 0.633, 1.0) } ;
static GLfloat emeraldShine = 76.8;



static vec3 lookPos = vec3( 0, 0, 0);
static vec3 UpVec = normalize(vec3(0,1, 0));

int Index = 0;

float height = 2.0;
float rad = 0.0;
float curangle = 0;
float delangle = 0;
float deldelangle = 3.14/180;

float lightheight = -2.0;
float lightrad = 1.0;
float curanglelight = 0.0;

static vec3 camPos = vec3 ( rad*cos(curangle), rad*sin(curangle), height);
vec4 light0_pos = vec4 ( camPos, 1.0);
vec4 light1_pos = vec4(lightrad*cos(curanglelight), lightrad*sin(curanglelight), lightheight, 1.0); 



//The make triangle function also calculates the normals and the triangle colors.
void maketriangle ( int a, int b, int c )
{
  finalpoints[Index] = points[a-1];
  normals[Index] = vec4( vertexnorms[a-1], 1.0);
  Index++;

  finalpoints[Index] = points[b-1];
  normals[Index] = vec4( vertexnorms[b-1], 1.0);
  Index++;

  finalpoints[Index] = points[c-1];
  normals[Index] = vec4( vertexnorms[c-1], 1.0);
  Index++;

} 

int Index2 = 0;

void
addVertex(float a, float b, float c)
{
  points[Index2] = vec4(a, b, c, 1.0);
  Index2++;
} 


//This function runs through the faces to comute their normals and adds them to a running total for each vertex.
//Each total is then divided by the total number of times visited.
void
ComputeVertexNormals()
{
  for( int i = 0; i < 192; i++)
    {
      vertexnorms[i] = 0;
    }
  
  for( int b = 0; b < 64; b++)
    {
      points[64+b] = vec4(points[b].x + 1.0, points[b].y, points[b].z, 1);
    }

  for( int c = 0; c < 64; c++)
    {
      points[128+c] = vec4(points[c].x - 1.0, points[c].y, points[c].z, 1);
    }

  for( int d = 0; d < 128; d++)
    {
      faces[128+d] = vec3(faces[d].x + 64, faces[d].y + 64, faces[d].z + 64);
      faces[256+d] = vec3(faces[d].x + 128, faces[d].y + 128, faces[d].z + 128);
    }
  
  for( int s = 0; s < 384; s++)
    {
      vec3 no = normalize(cross(( points[(int)faces[s].z - 1] - points[(int)faces[s].x - 1]) , (points[(int)faces[s].y - 1] - points[(int)faces[s].x - 1])));
      vertexnorms[(int)faces[s].x - 1] += no;
      vertexnorms[(int)faces[s].y - 1] += no;
      vertexnorms[(int)faces[s].z - 1] += no;
      
    }

  for( int k = 0; k < 192; k++)
    {
      vertexnorms[k] = vertexnorms[k]/length(vertexnorms[k]);
      float kd = dot(vertexnorms[k], normalize(light0_pos));
    }
  
  for( int t = 0; t < 384; t++)
    {
      maketriangle(faces[t].x, faces[t].y, faces[t].z);
    }


}

void
init( void )
{
  std::ifstream infile("sprtrd.smf");
  int facesindex = 0;

  for( std::string line; getline( infile, line ); )
  {
    std::string token;
    int pos1, pos2, pos3;
    float p1, p2, p3;
    char* useline = new char[line.length()+1];
    strcpy(useline, line.c_str());
    useline[line.length()] = '\0';
    std::stringstream ss(useline);
    std::string s;
    getline(ss, s, ' ');
    if( s.c_str() != NULL) {
      if( !strcmp(s.c_str(), "v")){
	getline(ss, s, ' ');
	p1 = atof(s.c_str())/12;
	getline(ss, s, ' ');
	p2 = atof(s.c_str())/12;
	getline(ss, s, ' ');
	p3 = atof(s.c_str())/12;
	addVertex(p1, p2, p3);
      }
      else if(!strcmp(s.c_str(), "f")) {
	getline(ss, s, ' ');
	pos1 = atoi(s.c_str());
	getline(ss, s, ' ');
	pos2 = atoi(s.c_str());
	getline(ss, s, ' ');
	pos3 = atoi(s.c_str());
	faces[facesindex] = vec3(pos1, pos2, pos3);
	facesindex++;
      }
    }
  }

  ComputeVertexNormals();


  // Create a vertex array object
  GLuint vao;
  glGenVertexArrays ( 1, &vao );
  glBindVertexArray ( vao );



  // Create and initialize a buffer object
  GLuint buffer, render_buf, depth_buf;
  glGenBuffers( 1, &buffer );
  glBindBuffer( GL_ARRAY_BUFFER, buffer );
  glBufferData( GL_ARRAY_BUFFER, sizeof(finalpoints) +
		sizeof(normals), NULL, GL_STATIC_DRAW );
  glBufferSubData( GL_ARRAY_BUFFER, 0,
		   sizeof(finalpoints), finalpoints );
  glBufferSubData( GL_ARRAY_BUFFER, sizeof(finalpoints),
		   sizeof(normals), normals );


  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glGenRenderbuffers(1, &render_buf);
  glBindRenderbuffer(GL_RENDERBUFFER, render_buf);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA4, 500, 500);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, render_buf);
  
  glGenRenderbuffers(1, &depth_buf);
  glBindRenderbuffer(GL_RENDERBUFFER, depth_buf);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 500, 500);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
						     GL_RENDERBUFFER, depth_buf);


  // Load shaders and use the resulting shader program
  GLuint program = InitShader( "vshader21.glsl", "fshader21.glsl" );
  glUseProgram( program );

  // set up vertex arrays
  GLuint vPosition = glGetAttribLocation( program, "vPosition" );
  glEnableVertexAttribArray( vPosition );
  glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,
			 BUFFER_OFFSET(0) );

  GLuint vNorm = glGetAttribLocation( program, "vNorm" );
  glEnableVertexAttribArray( vNorm );
  glVertexAttribPointer( vNorm, 4, GL_FLOAT, GL_FALSE, 0,
			 BUFFER_OFFSET(sizeof(finalpoints)) );
  transLoc = glGetUniformLocation( program, "transformation" ); 
  lp0Loc = glGetUniformLocation( program, "lightpos0" ); 
  lp1Loc = glGetUniformLocation( program, "lightpos1" ); 
  projLoc = glGetUniformLocation( program, "projection");
  eyeLoc =  glGetUniformLocation( program, "eyePos");
  ambLoc = glGetUniformLocation( program, "AmbientProduct");
  difLoc = glGetUniformLocation( program, "DiffuseProduct");
  specLoc = glGetUniformLocation( program, "SpecularProduct");
  shineLoc = glGetUniformLocation( program, "Shininess");
  shadeLoc = glGetUniformLocation(program, "shadeswitch");

  glEnable(GL_DEPTH_TEST);

}

//----------------------------------------------------------------------------

void
display( void )
{
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  //glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  //THIS IS WHAT WE CAN'T SEE
  
  mat4 model_view = LookAt(camPos, lookPos, UpVec);
  mat4 projection = Perspective(60, 1, 0.1, 200);

  glUniformMatrix4fv(transLoc, 1, GL_TRUE,
  		     model_view); 
  glUniform4fv(lp0Loc, 1,
  		     light0_pos); 
  glUniform4fv(lp1Loc, 1,
  		     light1_pos);
  glUniform3fv(eyeLoc, 1,
		     camPos);
  glUniformMatrix4fv(projLoc, 1, GL_TRUE,
		     projection);

  
		     
  glUniform1f(shadeLoc, 101);

  glUniform4fv(difLoc, 1,
	       vec4(0,1,0, 1));
  glDrawArrays( GL_TRIANGLES, 0, 384 );

  glUniform4fv(difLoc, 1,
	       vec4(0,0,1, 1));
  glDrawArrays( GL_TRIANGLES, 384, 384 );

  glUniform4fv(difLoc, 1,
	       vec4(1,0,0,1));
  glDrawArrays( GL_TRIANGLES, 768, 384 );


  //THIS IS WHAT WE CAN SEE
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  //glDrawBuffer(GL_BACK);
  
  glClear( GL_COLOR_BUFFER_BIT
  	   | GL_DEPTH_BUFFER_BIT );
  glUniform1f(shadeLoc, shading);

  if( matchoose == 0) //silver
    {
      glUniform4fv(ambLoc, 1,
		     silver[0]);
      glUniform4fv(difLoc, 1,
		     silver[1]);
      glUniform4fv(specLoc, 1,
		     silver[2]);
      glUniform1f(shineLoc, silverShine);


    }
  else if(matchoose == 1) //turquoise
    {
      glUniform4fv(ambLoc, 1,
		     turq[0]);
      glUniform4fv(difLoc, 1,
		     turq[1]);
      glUniform4fv(specLoc, 1,
		     turq[2]);
      glUniform1f(shineLoc, turqShine);


    }
  else // Emerald
    {
      glUniform4fv(ambLoc, 1,
		     emerald[0]);
      glUniform4fv(difLoc, 1,
		     emerald[1]);
      glUniform4fv(specLoc, 1,
		     emerald[2]);
      glUniform1f(shineLoc, emeraldShine);


    }

  
  glDrawArrays( GL_TRIANGLES, 0, 384 );



  if( matchoose1 == 0) //silver
    {
      glUniform4fv(ambLoc, 1,
		     silver[0]);
      glUniform4fv(difLoc, 1,
		     silver[1]);
      glUniform4fv(specLoc, 1,
		     silver[2]);
      glUniform1f(shineLoc, silverShine);


    }
  else if(matchoose1 == 1) //turquoise
    {
      glUniform4fv(ambLoc, 1,
		     turq[0]);
      glUniform4fv(difLoc, 1,
		     turq[1]);
      glUniform4fv(specLoc, 1,
		     turq[2]);
      glUniform1f(shineLoc, turqShine);


    }
  else // Emerald
    {
      glUniform4fv(ambLoc, 1,
		     emerald[0]);
      glUniform4fv(difLoc, 1,
		     emerald[1]);
      glUniform4fv(specLoc, 1,
		     emerald[2]);
      glUniform1f(shineLoc, emeraldShine);


    }

  
  glDrawArrays( GL_TRIANGLES, 384, 384 );

  if( matchoose2 == 0) //silver
    {
      glUniform4fv(ambLoc, 1,
		     silver[0]);
      glUniform4fv(difLoc, 1,
		     silver[1]);
      glUniform4fv(specLoc, 1,
		     silver[2]);
      glUniform1f(shineLoc, silverShine);


    }
  else if(matchoose2 == 1) //turquoise
    {
      glUniform4fv(ambLoc, 1,
		     turq[0]);
      glUniform4fv(difLoc, 1,
		     turq[1]);
      glUniform4fv(specLoc, 1,
		     turq[2]);
      glUniform1f(shineLoc, turqShine);


    }
  else // Emerald
    {
      glUniform4fv(ambLoc, 1,
		     emerald[0]);
      glUniform4fv(difLoc, 1,
		     emerald[1]);
      glUniform4fv(specLoc, 1,
		     emerald[2]);
      glUniform1f(shineLoc, emeraldShine);


    }

  
  glDrawArrays( GL_TRIANGLES, 768, 384 );
  
  glutSwapBuffers();
}

void 
keyboard( unsigned char key, int x, int y)
{
  switch ( key ) {
  case 'h': //Height Up
    height += 0.10;
    break;
  case 'n':
    height -= 0.10;
    break;
  case 'r':
    rad += 0.10;
    break;
  case 'f':
    rad -= 0.10;
    break;
  case 's':
    delangle += deldelangle;
    break;
  case 'x':
    delangle -= deldelangle;
    break;
  case 'e':
    lightheight += 0.10;
    light1_pos = vec4(lightrad*cos(curanglelight), lightrad*sin(curanglelight), lightheight, 1.0); 
    break;
  case 'd':
    lightheight -= 0.10;
    light1_pos = vec4(lightrad*cos(curanglelight), lightrad*sin(curanglelight), lightheight, 1.0); 
    break;
  case 'q':
    lightrad += 0.10;
    light1_pos = vec4(lightrad*cos(curanglelight), lightrad*sin(curanglelight), lightheight, 1.0); 
    break;
  case 'a':
    lightrad -= 0.10;
    light1_pos = vec4(lightrad*cos(curanglelight), lightrad*sin(curanglelight), lightheight, 1.0); 
    break;
  case 'o':
    curanglelight += deldelangle;
    light1_pos = vec4(lightrad*cos(curanglelight), lightrad*sin(curanglelight), lightheight, 1.0); 
    break;
  case 'l':
    curanglelight -= deldelangle;
    light1_pos = vec4(lightrad*cos(curanglelight), lightrad*sin(curanglelight), lightheight, 1.0); 
    break;
  case 'z':
    rad = 0.0;
    height = 2.0;
    curangle = 0;
    break;
    
    
    
  }

}
int gater = 0;
void idleFunc(void) {
  gater++;
  if (gater == 50){
    gater = 0;
    curangle += delangle;
    camPos = vec3 (rad * cos(curangle), rad * sin(curangle), height);
    light0_pos = vec4(camPos, 1.0);
    gater = 0;
  }
  glutPostRedisplay();

}

void menu2(int value) {
  
  if (value < 2)
    {   active = value;}
  else if( value < 5)
    { matchoose = value - 2; }
  else
    { shading = value - 5; }
    glutPostRedisplay();
  
}

void createMainMenu(void) {

  int sub = glutCreateMenu(menu2);

  int sub2 = glutCreateMenu(menu2);
  glutAddMenuEntry("Gouraud", 5);
  glutAddMenuEntry("Phong",6);
  
  glutCreateMenu(menu2);
  glutAddSubMenu("Change Shading Algorithm ->", sub2);
  

  glutAttachMenu(GLUT_RIGHT_BUTTON);

}

void mouseClicks(int button, int state, int x, int y) {
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
      GLfloat array[3];
      glBindFramebuffer(GL_FRAMEBUFFER, fbo);
      glReadBuffer(GL_COLOR_ATTACHMENT0); 
      glReadPixels(x,y,1,1, GL_RGB, GL_FLOAT, array);
      if((int)array[0] == 1)
	{
	  matchoose2 = (matchoose2 + 1) % 3;
	}
      if( (int)array[1] == 1)
	{
	  matchoose = (matchoose + 1) % 3;
	}
      if( (int)array[2] == 1)
	{
	  matchoose1 = (matchoose1 + 1) % 3;
	}
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
}

int
main( int argc, char **argv )
{
  printf("Welcome!\nThe operative keys are as follows:\n\tThe s and x keys can be used to speed up and slow down the rotation of the figure. By pressing the x key once on startup, the object will stop rotating. In this way, the user can control the starting and stopping of the rotation animation.\n\tUsing h and n, the user can increase and decrease respectively the height of the camera.\n\tBy using r and f, the user can increase and decrease respectively the radius from the camera to the lookAt point.\n\tThe options to switch between parallel and perspective projections are located in a right click menu.\n\tThe Options to switch between the materials and shading styles are also located in this right click menu.\n\n\tIn order to control the rotating light, it is o to rotate forwards and l to rotate backwards, e to increase the height, d to decrease the height, q to increase the radius, and a to decrease the radius.\nThe z key is used to reset the camera position.");


    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( 500, 500 );

    mainwindow = glutCreateWindow( "Assignment8" );
    glewExperimental=GL_TRUE; 
    glewInit();    
    
    init();

    glutIdleFunc(idleFunc);
    glutDisplayFunc( display );
    glutKeyboardFunc( keyboard );
    glutMouseFunc(mouseClicks);
    createMainMenu();

    glutMainLoop();
    return 0;
}
