#version 120

attribute vec4 vPosition;
attribute vec4 vNorm;

varying vec3 fN;
varying vec3 fE;
varying vec3 fL;
varying vec3 fL2;
varying vec3 color;

uniform mat4 transformation;
uniform vec4 lightpos0;
uniform vec4 lightpos1;
uniform mat4 projection;
uniform vec3 eyePos;
uniform float shadeswitch;
uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform float Shininess;


void
main()
{
    if (shadeswitch == 1) {
       fN = vNorm.xyz;
       fE = eyePos - vPosition.xyz;

       // Light defined in world coordinates
       if ( lightpos0.w != 0.0 ) 
       {
		fL = lightpos0.xyz - vPosition.xyz;
       }
       else {
 	 	fL = lightpos0.xyz;
	} 
     	// Light defined in world coordinates
     	if ( lightpos1.w != 0.0 ) {
     	     fL2 = lightpos1.xyz - vPosition.xyz;
 	     } 
	     else {
 	       	  fL2 = lightpos1.xyz;
 	       	  }
        gl_Position = projection * transformation * vPosition;
	}
	if(shadeswitch == 0)
	{
		vec3 pos = (transformation * vPosition).xyz;
		vec3 L = normalize( lightpos0.xyz - pos);
		vec3 L2 = normalize( lightpos1.xyz - pos);
		vec3 E = normalize( -pos);
		vec3 H = normalize( L+E);
		vec3 H2 = normalize( L2+E);

		vec3 N = (normalize( transformation * vNorm)).xyz;
		vec3 ambient = AmbientProduct.xyz;
		float Kd = max(dot(L,N), 0.0) + max(dot(L2,N),0.0);
		vec3 diffuse = Kd*DiffuseProduct.xyz;
		float Ks = pow( max(dot(N,H), 0.0), Shininess) + pow( max(dot(N,H2), 0.0), Shininess);
		vec3 specular = Ks*SpecularProduct.xyz;
		if( dot(L,N) < 0.0 && dot(L2,N) < 0.0)
		{
			specular = vec3(0.0,0.0,0.0);
		}
		color = ambient + diffuse + specular;
		gl_Position = projection * transformation * vPosition;	


	}
	if(shadeswitch == 101)
	{
		gl_Position = projection * transformation * vPosition;
	}
}
